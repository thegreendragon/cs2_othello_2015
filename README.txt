Part a: Each member's contributions.

Taylor: Taylor made the original heuristic and its helper functions.  He made it so
the scoring system favored corner moves, while avoiding its neighboring tiles.  He
also made the heuristic function value diagonal pieces and edge pieces higher than other
pieces on the board.  He also made modifications to the original minimax algorithm, which
was originally made by David.  He later made a memoization extension of the minimax
algorithm that sped up the computation time of move, as well as an end-game strategy.

David Pena: Implemented minimax, and made the doMove method faster. Also
added debugging preprocessor directives throughout the code for future
debugging. Implemented AlphaBeta pruning algorithm.


Part b. Tournament improvements

We added in memoization that increased the speed of our computations by at least a second
against Better Player, while maintaining the same results.
We felt that optimizing the speed of these computations made a depth of four more reasonable 
to implement, where time is a constraint in the tournament.  Currently, our games finish 
in 20 to 30 seconds against BetterPlayer.  Even though the time constraint in the tournament 
is on the order of minutes, it still is nice to make the program run a couple of seconds faster.
Unfortunately, we cannot go past a depth of four because of memory limitations.  
As implied earlier, we also increased the depth recursion to four.  These 
two changes helped us beat BetterPlayer in our trial runs, while still playing the game 
in a reasonable amount of time.  We also changed our end-game strategy and heuristic function.
Our end game strategy prioritizes the number of pieces rather than the type of pieces
(corners, edges, etc.).  Our heuristic function, on the other hand, was changed to increase 
the priority of corners, decrease the priority of edges, increase the priority of diagonal pieces,
and decrease the priority of pieces around the corners.  These changes allowed us to beat
BetterPlayer every time when using a depth of 4.