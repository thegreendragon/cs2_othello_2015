#include "board.h"
#include <cmath>
#include <iostream>

// memo_type will allow us to hash the string input to align
// with its output for memoization()
typedef unordered_map<string, int> memo_type;

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}


/**
 *  Computes a favorability score for a given move.
 *
 *  @param testBoard Board to test move on
 *  @param m         Move to compute score for
 *  @param side      Color of piece of player
 *
 *  @return Score for given move
 */
int Board::heuristic(Board *testBoard, Move *m, Side side)
{
    testBoard->doMove(m, side);
    return heuristicHelper(testBoard, side);
}

/**
 *  Computes the favorability score for a certain side without
 *  considering the other side's pieces.
 *
 *  @param testBoard Board to test move on
 *  @param side      Color of piece of player
 *
 *  @return Score for given side
 */
int Board::heuristicHelper(Board *testBoard, Side side)
{
    int temp = 0;
    /* ----- Score constants ----- */
    /** Moves obtaining corners */
    const int move_corner = 100;
    /** Moves obtaining edges */
    const int move_edges = 10;
    /** Moves obtaining diagonal pieces */
    const int move_diagonal = 5;
    /** Moves just obtaining pieces */
    const int move_obtain_pieces = 1;
    /** Moves surrounding corners */
    const int move_surround_corner = -50;
    
    // Looks at upper-left corner.
    if (testBoard->get(side, 0, 0))
        temp += move_corner;
    else
    {
        // Will be undesired -3 to account for +3 for diagonal
        if (testBoard->get(side, 1, 1))
            temp += move_surround_corner - move_diagonal;
        if (testBoard->get(side, 0, 1))
            temp += move_surround_corner;
        if (testBoard->get(side, 1, 0))
            temp += move_surround_corner;
    }

    // Looks at lower-left corner.
    if (testBoard->get(side, 0, 7))
        temp += move_corner;
    else
    {
        // Will be undesired -3 to account for +3 for diagonal
        if (testBoard->get(side, 1, 6))
            temp += move_surround_corner - move_diagonal;
        if (testBoard->get(side, 0, 6))
            temp += move_surround_corner;
        if (testBoard->get(side, 1, 7))
            temp += move_surround_corner;
    }

    // Looks at upper-right corner.
    if (testBoard->get(side, 7, 0))
        temp += move_corner;
    else
    {
        // Will be undesired -3 to account for +3 for diagonal
        if (testBoard->get(side, 6, 1))
            temp += move_surround_corner - move_diagonal;
        if (testBoard->get(side, 6, 0))
            temp += move_surround_corner;
        if (testBoard->get(side, 7, 1))
            temp += move_surround_corner;
    }

    // Looks at lower-right corner.
    if (testBoard->get(side, 7, 7))
        temp += move_corner;
    else
    {
        // Will be undesired -3 to account for +3 for diagonal
        if (testBoard->get(side, 6, 6))
            temp += move_surround_corner - move_diagonal;
        if (testBoard->get(side, 7, 6))
            temp += move_surround_corner;
        if (testBoard->get(side, 6, 7))
            temp += move_surround_corner;
    }
    // Looks at left-edges.
    for (int i = 2; i < 6; i++)
    {
        if (testBoard->get(side, i, 0))
            temp += move_edges;
    }

    // Looks at right-edges.
    for (int i = 2; i < 6; i++)
    {
        if (testBoard->get(side, i, 7))
            temp += move_edges;
    }

    // Looks at upper-edges.
    for (int i = 2; i < 6; i++)
    {
        if (testBoard->get(side, 0, i))
            temp += move_edges;
    }

    // Looks at lower-edges.
    for (int i = 2; i < 6; i++)
    {
        if (testBoard->get(side, 7, i))
            temp += move_edges;
    }

    // Accounts for moves that give more pieces
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++)
        {
            if (testBoard->get(side, i, j))
            {
                // Diagonal piece
                if (i == j || i == (7 - j))
                    temp += move_diagonal;
                else
                    temp += move_obtain_pieces;
            }
        }
    }

    // Looks at upper-left corner.
    if (testBoard->get(side, 0, 0))
        temp += move_corner;
    else
    {
        // Will be undesired -3 to account for +3 for diagonal
        if (testBoard->get(side, 1, 1))
            temp += move_surround_corner - move_diagonal;
        if (testBoard->get(side, 0, 1))
            temp += move_surround_corner;
        if (testBoard->get(side, 1, 0))
            temp += move_surround_corner;
    }

    // Looks at lower-left corner.
    if (testBoard->get(side, 0, 7))
        temp += move_corner;
    else
    {
        // Will be undesired -3 to account for +3 for diagonal
        if (testBoard->get(side, 1, 6))
            temp += move_surround_corner - move_diagonal;
        if (testBoard->get(side, 0, 6))
            temp += move_surround_corner;
        if (testBoard->get(side, 1, 7))
            temp += move_surround_corner;
    }

    // Looks at upper-right corner.
    if (testBoard->get(side, 7, 0))
        temp += move_corner;
    else
    {
        // Will be undesired -3 to account for +3 for diagonal
        if (testBoard->get(side, 6, 1))
            temp += move_surround_corner - move_diagonal;
        if (testBoard->get(side, 6, 0))
            temp += move_surround_corner;
        if (testBoard->get(side, 7, 1))
            temp += move_surround_corner;
    }

    // Looks at lower-right corner.
    if (testBoard->get(side, 7, 7))
        temp += move_corner;
    else
    {
        // Will be undesired -3 to account for +3 for diagonal
        if (testBoard->get(side, 6, 6))
            temp += move_surround_corner - move_diagonal;
        if (testBoard->get(side, 7, 6))
            temp += move_surround_corner;
        if (testBoard->get(side, 6, 7))
            temp += move_surround_corner;
    }
    // Looks at left-edges.
    for (int i = 2; i < 6; i++)
    {
        if (testBoard->get(side, i, 0))
            temp += move_edges;
    }

    // Looks at right-edges.
    for (int i = 2; i < 6; i++)
    {
        if (testBoard->get(side, i, 7))
            temp += move_edges;
    }

    // Looks at upper-edges.
    for (int i = 2; i < 6; i++)
    {
        if (testBoard->get(side, 0, i))
            temp += move_edges;
    }

    // Looks at lower-edges.
    for (int i = 2; i < 6; i++)
    {
        if (testBoard->get(side, 7, i))
            temp += move_edges;
    }

    // Accounts for moves that give more pieces
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++)
        {
            if (testBoard->get(side, i, j))
            {
                // Diagonal piece
                if (i == j || i == (7 - j))
                    temp += move_diagonal;
                else
                    temp += move_obtain_pieces;
            }
        }
    }
    return temp;
}


/**
 *  Gets the score of the move for the given side using 
 *  the minimax algorithm looking the depth number of 
 *  moves into the future.
 *
 *  @param testboard Board to test the move on
 *  @param m         Move to test
 *  @param side      Side to test the move for
 *  @param depth     Number of moves to look into the future.
 *
 *  @return Score for given board, move, and side.
 */
int Board::minimax(Board *testboard, Move *m, Side side, int depth)
{
    // Count number of moves on board
    int terminal = testboard->countBlack() + testboard->countWhite();
    
    if (terminal >= 63)
    {
        testboard->doMove(m, side);
        if (testboard->countBlack() > testboard->countWhite() && side == BLACK)
            return (5000 + testboard->countBlack());
        else if (testboard->countBlack() < testboard->countWhite() && side == WHITE)
            return (5000 + testboard->countWhite());
        else if (testboard->countBlack() > testboard->countWhite() && side == WHITE)
            return testboard->countWhite();
        else if (testboard->countBlack() < testboard->countWhite() && side == BLACK)
            return testboard->countBlack();
        else
            return 2500;
    }

    if (depth <= 0)
        return heuristic(testboard, m, side);

    Board *newTestBoard = testboard->copy();
    newTestBoard->doMove(m, side);
    
    // Side that will make next move
    Side otherSide = side == BLACK ? WHITE : BLACK;
    
    // Create a list of possible moves
    std::vector<Move *> possibleMoves = getMoves(newTestBoard, otherSide);
    
    // Best score so far
    int best_score = 10000;
    
    // Iterate over all the possible moves and find the max
    std::vector<Move *>::iterator i;
    for (i = possibleMoves.begin(); i != possibleMoves.end(); i++)
    {
        Move *curMove = *i;
        Board *nextTestBoard = newTestBoard->copy();
        
        // Score of making the current move
        int cur_score = -minimax(nextTestBoard, curMove, otherSide, depth - 1);
        
        #ifdef DEBUG
        std::cerr << "MINIMAX: Testing move: (" << curMove->getX() << ", "
                  << curMove->getY() << "). Score: " << cur_score << std::endl;
        #endif
        
        best_score = min(best_score, cur_score);
        
        // Done with next test board
        delete nextTestBoard;
    }

    // Considers case where other side has no moves.
    if (!newTestBoard->hasMoves(otherSide))
    {
        Board *nextTestBoard = newTestBoard->copy();
        best_score = -minimax(nextTestBoard, NULL, otherSide, depth - 1);
        delete nextTestBoard;
    }

    // Done with the new test board
    delete newTestBoard;
    return best_score;
}

/**
 *  Computes the maximum score move for the given parameters
 *  using AlphaBeta testing
 *
 *  @param testboard        Board to get moves for
 *  @param m                Move to apply to board
 *  @param side             Side to get moves for
 *  @param maximizingPlayer Whether this side is the one we're maximizing for.
 *  @param depth            Number of moves to look into the future for
 *  @param alpha            Alpha value (initially should be -inf)
 *  @param beta             Beta value (initially should be +inf)
 *
 *  @return Maximum score for the given parameters.
 */
int Board::alphaBeta(Board *testboard, Move *m, Side side,
                     bool maximizingPlayer, int depth, int alpha, int beta)
{
    // Count number of moves on board
    int terminal = testboard->countBlack() + testboard->countWhite();
    
    // Base case, depth of 0 or only one move left
    if (depth <= 0 || terminal >= 63)
        return heuristic(testboard, m, side);
    
    Board *newTestBoard = testboard->copy();
    newTestBoard->doMove(m, side);
    
    // Side that will make next move
    Side otherSide = side == BLACK ? WHITE : BLACK;
    
    // Get a list of possible moves
    std::vector<Move *> possibleMoves = getMoves(newTestBoard, otherSide);
    
    int best_score;
    
    if (maximizingPlayer) {
        best_score = -10000;
        std::vector<Move *>::iterator i;
        for (i = possibleMoves.begin(); i != possibleMoves.end(); i++)
        {
            Move *curMove = *i;
            Board *nextTestBoard = newTestBoard->copy();
            
            best_score = max(best_score, alphaBeta(nextTestBoard, curMove,
                                                   otherSide, false, depth - 1,
                                                   alpha, beta));
            alpha = max(alpha, best_score);
            
            #ifdef DEBUG
            std::cerr << "AlphaBeta: Testing move: (" << curMove->getX() << ", "
            << curMove->getY() << "). Score: " << best_score << " Alpha: "
            << alpha << " Beta: " << beta << std::endl;
            #endif
            
            // Done with next test board
            delete nextTestBoard;
            
            // Beta cutoff
            if (beta < alpha)
                break;
        }
    }
    else
    {
        best_score = 10000;
        std::vector<Move *>::iterator i;
        for (i = possibleMoves.begin(); i != possibleMoves.end(); i++)
        {
            Move *curMove = *i;
            Board *nextTestBoard = newTestBoard->copy();
            best_score = min(best_score, alphaBeta(nextTestBoard, curMove,
                                                   otherSide, true, depth - 1,
                                                   alpha, beta));
            beta = min(beta, best_score);
            
            #ifdef DEBUG
            std::cerr << "AlphaBeta: Testing move: (" << curMove->getX() << ", "
            << curMove->getY() << "). Score: " << best_score << " Alpha: "
            << alpha << " Beta: " << beta << std::endl;
            #endif
            
            // Done with next test board
            delete nextTestBoard;
            
            // Alpha cutoff
            if (beta < alpha)
                break;
        }
    }
    
    // Done with the new test board
    delete newTestBoard;
    
    return best_score;
}

/**
 *  Gets the score of the move for the given side using
 *  the minimax algorithm looking the depth number of 
 *  moves into the future, while also using memoization.
 *
 *  @param testboard Board to test the move on
 *  @param m         Move to test
 *  @param side      Side to test the move for
 *  @param depth     Number of moves to look into the future.
 *  @param &memo     Unordered map for memoization of board positions.
 *
 *  @return Score for given board, move, and side.
 */
int Board::memominimax(Board *testboard, Move *m, Side side, int depth, 
                       memo_type &memo)
{
    // Count number of moves on board
    int terminal = testboard->countBlack() + testboard->countWhite();
    
    if (terminal >= 63)
    {
        testboard->doMove(m, side);
        if (testboard->countBlack() > testboard->countWhite() && side == BLACK)
            return (5000 + testboard->countBlack());
        else if (testboard->countBlack() < testboard->countWhite() && side == WHITE)
            return (5000 + testboard->countWhite());
        else if (testboard->countBlack() > testboard->countWhite() && side == WHITE)
            return testboard->countWhite();
        else if (testboard->countBlack() < testboard->countWhite() && side == BLACK)
            return testboard->countBlack();
        else
            return 2500;
    }

    if (depth <= 0)
        return heuristic(testboard, m, side);

    Board *newTestBoard = testboard->copy();
    newTestBoard->doMove(m, side);
    // String describing the board layout.
    string board_layout = newTestBoard->black.to_string() + 
        newTestBoard->taken.to_string() + string(depth, 'd');
    if (memo.count(board_layout) > 0)
    {
        return memo[board_layout];
    }
    
    // Side that will make next move
    Side otherSide = side == BLACK ? WHITE : BLACK;
    
    // Get vector of possible moves
    std::vector<Move *> possibleMoves = getMoves(newTestBoard, otherSide);
    
    // Best score so far
    int best_score = 10000;
    
    // Iterate over all the possible moves and find the max
    std::vector<Move *>::iterator i;
    for (i = possibleMoves.begin(); i != possibleMoves.end(); i++)
    {
        Move *curMove = *i;
        Board *nextTestBoard = newTestBoard->copy();
        
        // Score of making the current move
        int cur_score = -memominimax(nextTestBoard, curMove, otherSide,
                                     depth - 1, memo);
        
        #ifdef DEBUG
        std::cerr << "MemoMinimax: Testing move: (" << curMove->getX() << ", "
                  << curMove->getY() << "). Score: " << cur_score << std::endl;
        #endif
        
        best_score = min(best_score, cur_score);
        
        // Done with next test board
        delete nextTestBoard;
    }
    
    // Considers case where other side has no moves.
    if (!newTestBoard->hasMoves(otherSide))
    {
        Board *nextTestBoard = newTestBoard->copy();
        best_score = -memominimax(nextTestBoard, NULL, otherSide, depth - 1,
                                  memo);
        delete nextTestBoard;
    }

    // Memoizes the best score for the given board layout.
    memo[board_layout] = best_score;
    // Done with the new test board
    delete newTestBoard;
    return best_score;
}

/**
 *  Gets a vector of possible moves for the given board and side
 *
 *  @param testBoard Board to get moves for
 *  @param otherSide Side to get the moves for
 *
 *  @return Vector of possible moves
 */
std::vector<Move *> Board::getMoves(Board *testBoard, Side otherSide)
{    // Create a list of possible moves
    std::vector<Move *> possibleMoves;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            Move *testmove = new Move(i, j);
            if (testBoard->checkMove(testmove, otherSide))
                possibleMoves.push_back(testmove);
            else
                delete testmove;
        }
    }
    
    return possibleMoves;
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
