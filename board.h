#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <string>
#include <vector>
#include "common.h"
#include <unordered_map>
using namespace std;

/**
 * @brief Packages the score and instruction string the align function returns
 */

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    // memo_type will allow us to hash the string input to align
    // with its output for memoization()
    typedef unordered_map<string, int> memo_type;

    Board();
    ~Board();
    Board *copy();
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int countTotal();

    // Computes a favorability score for a given move.
    int heuristic(Board *testBoard, Move *m, Side side);
    int heuristicHelper(Board *testBoard, Side side);
    int minimax(Board *testboard, Move *m, Side side, int depth);
    int memominimax(Board *testboard, Move *m, Side side, int depth, 
                    memo_type &memo);
    int alphaBeta(Board *testboard, Move *m, Side side,
                  bool maximizingPlayer, int depth, int alpha, int beta);
    vector<Move *> getMoves(Board *testBoard, Side otherSide);
    
    void setBoard(char data[]);
};

#endif
