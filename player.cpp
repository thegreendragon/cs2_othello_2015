#include "player.h"
#include <assert.h>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    // Use alphaBeta over minimax and heuristic
    useAlphaBeta = false;
    
    // Use memoizing minimax
    useMemoMinimax = true;
    
    // Use minimax over heuristic
    useMinimax = false;

    // Variables that stores the side of each AI.
    mySide = side;
    otherSide = side == BLACK ? WHITE : BLACK;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/**
 *  Sets the given board as the current board only if testingMinimax is
 *  true. Used to prevent modifying private variables unless testing.
 *
 *  @param board Board to set as the current board
 */
void Player::setTestBoard(Board *board)
{
    this->board = *board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
    int best_move_x = -1, best_move_y = -1, max_score = -10000;
    // Perform the opponent's move
    board.doMove(opponentsMove, otherSide);
    
    if (board.hasMoves(mySide))
    {
        for (int i = 0; i < 8; i++) 
        {
            for (int j = 0; j < 8; j++) 
            {
                #ifdef DEBUG
                std::cerr << "Testing move: (" << i << ", " << j << ")"
                          << std::endl;
                #endif
                
                // Go to the next iteration if the move is not valid
                Move *testmove = new Move(i, j);
                if (board.checkMove(testmove, mySide) == false)
                {
                    delete testmove;
                    continue;
                }
                
                #ifdef DEBUG
                std::cerr << std::endl << "Testing valid move: (" << i
                          << ", " << j << ")" << std::endl;
                #endif
                
                // Get a copy of the board to perform the tests on
                Board *testBoard = board.copy();
                
                // Score of current move using alphaBeta algorithm
                int score;
                if (useAlphaBeta)
                    score = board.alphaBeta(testBoard, testmove, mySide,
                                            true, 4, -10000, 10000);
                // Score of current move using memoizing minimax
                else if (useMemoMinimax)
                    score = board.memominimax(testBoard, testmove,
                                              mySide, 4, memo);
                // Score of current move using minimax
                else if (useMinimax)
                    score = board.minimax(testBoard, testmove, mySide, 2);
                // Use simple heuristic
                else
                    score = board.heuristic(testBoard, testmove, mySide);

                
                #ifdef DEBUG
                std::cerr << "Testing minimax: "
                          << (testingMinimax == true ? 't' : 'f')
                          << " Score: " << score << "\n\n";
                #endif
                
                // Current move is better than best move
                if (score > max_score)
                {
                    max_score = score;
                    best_move_x = i;
                    best_move_y = j;
                }
                
                // Done with test variables
                delete testBoard;
                delete testmove;
            }
        }
        
        #ifdef DEBUG
        std::cerr << "Best move: (" << best_move_x << ", "
                  << best_move_y << ")" << std::endl;
        #endif
        
        assert(best_move_x != -1 && best_move_y != -1);
        
        // Make the move with the highest score
        Move *actual = new Move(best_move_x, best_move_y);
        board.doMove(actual, mySide);
        
        return actual;
    }
    
    return NULL;
}
