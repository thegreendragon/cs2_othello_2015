#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

// memo_type will allow us to hash the string input to align
// with its output for memoization()
typedef unordered_map<string, int> memo_type;
class Player {

// Private variables that store the board state and piece-associations.
private:
    Board board;
    Side mySide;
    Side otherSide;
    memo_type memo;
    /** Set to true to use alphaBeta, false otherwise. Takes
      * precedence over other options */
    bool useAlphaBeta;
    /** Set to true to use memoizing minimax, false otherwise. Takes
      * precedence over useMinimax */
    bool useMemoMinimax;
    /** Set to true to use minimax, false to use only heuristic */
    bool useMinimax;
    
public:
    Player(Side side);
    ~Player();
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    // Sets the given board as the current board if testing minimax
    void setTestBoard(Board *board);
};

#endif
